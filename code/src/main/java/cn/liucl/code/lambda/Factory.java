package cn.liucl.code.lambda;

public interface Factory {

     Object getObject();
}
