package cn.liucl.code.leetcode;

import java.util.Arrays;

public class Topic1480 {

    public static void main(String[] args) {
        //int[] arr = {1, 2, 3, 4};
        //int[] arr = {1,1,1,1,1};
        int[] arr = {3,1,2,10,1};
        int[] solution = runningSum(arr);
        System.out.println(Arrays.toString(solution));
    }

    /**
     * 给你一个数组 nums 。数组「动态和」的计算公式为：runningSum[i] = sum(nums[0]…nums[i]) 。
     * <p>
     * 请返回 nums 的动态和。
     * <p>
     * <p>
     * 示例 1：
     * 输入：nums = [1,2,3,4]
     * 输出：[1,3,6,10]
     * 解释：动态和计算过程为 [1, 1+2, 1+2+3, 1+2+3+4] 。
     * <p>
     * 示例 2：
     * 输入：nums = [1,1,1,1,1]
     * 输出：[1,2,3,4,5]
     * 解释：动态和计算过程为 [1, 1+1, 1+1+1, 1+1+1+1, 1+1+1+1+1] 。
     * <p>
     * 示例 3：
     * 输入：nums = [3,1,2,10,1]
     * 输出：[3,4,6,16,17]
     * <p>
     * 提示：
     * <p>
     * 1 <= nums.length <= 1000
     * -10^6 <= nums[i] <= 10^6
     */
    public static int[] runningSum(int[] nums) {
        for (int i = 0; i < nums.length; i++) {
            if (i > 0) {
                nums[i] += nums[i - 1];
            }
        }
        return nums;
    }
}
