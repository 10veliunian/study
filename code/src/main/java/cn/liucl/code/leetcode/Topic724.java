package cn.liucl.code.leetcode;

import java.util.Arrays;
import java.util.HashMap;

public class Topic724 {

    public static void main(String[] args) {
        int[] arr = {-1, -1, 0, 0, -1, -1};
        int pivotIndex = pivotIndex(arr);
        System.out.println(pivotIndex);
    }

    /**
     * 给你一个整数数组 nums ，请计算数组的 中心下标 。
     * 数组 中心下标 是数组的一个下标，其左侧所有元素相加的和等于右侧所有元素相加的和。
     * 如果中心下标位于数组最左端，那么左侧数之和视为 0 ，因为在下标的左侧不存在元素。这一点对于中心下标位于数组最右端同样适用。
     * 如果数组有多个中心下标，应该返回 最靠近左边 的那一个。如果数组不存在中心下标，返回 -1 。
     * <p>
     * 示例 1：
     * 输入：nums = [1, 7, 3, 6, 5, 6]
     * 输出：3
     * 解释：中心下标是 3 。
     * 左侧数之和 sum = nums[0] + nums[1] + nums[2] = 1 + 7 + 3 = 11 ，
     * 右侧数之和 sum = nums[4] + nums[5] = 5 + 6 = 11 ，二者相等。
     * <p>
     * 示例 2：
     * 输入：nums = [1, 2, 3]
     * 输出：-1
     * 解释：数组中不存在满足此条件的中心下标。
     * <p>
     * 示例 3：
     * 输入：nums = [2, 1, -1]
     * 输出：0
     * 解释：中心下标是 0 。
     * 左侧数之和 sum = 0 ，（下标 0 左侧不存在元素），
     * 右侧数之和 sum = nums[1] + nums[2] = 1 + -1 = 0 。
     * <p>
     * 提示：
     * 1 <= nums.length <= 104
     * -1000 <= nums[i] <= 1000
     */
    //自己的想法
    public static int pivotIndex(int[] nums) {
        //倒序累加
        HashMap<Integer, Integer> reverse = new HashMap<>();
        for (int i = nums.length - 1; i >= 0; i--) {
            int value = nums[i];
            if (i < nums.length - 1) {
                value += reverse.get(i + 1);
            }

            reverse.put(i, value);
        }
        //统计正序，累加
        HashMap<Integer, Integer> forward = new HashMap<>();
        for (int i = 0; i < nums.length; i++) {
            int value = nums[i];
            if (i - 1 >= 0) {
                value += forward.get(i - 1);
            }
            //如果，同下标的和相同，即为中心下标
            if (value == reverse.get(i)) {
                return i;
            }
            forward.put(i, value);
        }
        return -1;
    }
    //其他解法
    /**
     * 题目仅说明是整数数组，无其他已知条件，因此考虑直接遍历数组。
     * <p>
     * 设索引 iii 对应变量「左侧元素相加和 sum_left 」和「右侧元素相加和 sum_right 」。
     * 遍历数组 nums ，每轮更新 sum_left 和 sum_right 。
     * 遍历中，遇到满足 sum_left == sum_right 时，说明当前索引为中心下标，返回即可。
     * 若遍历完成，仍未找到「中心下标」，则返回 -1 。
     * <p>
     * 初始化时，相当于索引 i=−1i = - 1i=−1 ，此时 sum_left = 0 , sum_right = 所有元素的和 。
     */
    public int pivotIndex1(int[] nums) {
        int sumLeft = 0, sumRight = Arrays.stream(nums).sum();
        for (int i = 0; i < nums.length; i++) {
            sumRight -= nums[i];
            // 若左侧元素和等于右侧元素和，返回中心下标 i
            if (sumLeft == sumRight)
                return i;
            sumLeft += nums[i];
        }
        return -1;
    }

}
