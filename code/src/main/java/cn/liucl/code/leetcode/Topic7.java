package cn.liucl.code.leetcode;

public class Topic7 {

    public static void main(String[] args) {
        int num = reverse1(-21474648);
        System.out.println(num);
    }

    /**
     * 给你一个 32 位的有符号整数 x ，返回将 x 中的数字部分反转后的结果。
     * 如果反转后整数超过 32 位的有符号整数的范围 [−231,  231 − 1] ，就返回 0。
     * 假设环境不允许存储 64 位整数（有符号或无符号）。
     * <p>
     * 示例 1：
     * 输入：x = 123
     * 输出：321
     * <p>
     * 示例 2：
     * 输入：x = -123
     * 输出：-321
     * <p>
     * 示例 3：
     * 输入：x = 120
     * 输出：21
     * <p>
     * 示例 4：
     * 输入：x = 0
     * 输出：0
     */
    //自己的想法
    public static int reverse(int x) {
        long returnNum = 0L;
        long xl = x;
        String returnStr = "";
        //1.正负判断
        if (x == 0) {
            return (int) returnNum;
        } else if (x < 0) {
            returnStr = "-";
            xl = -xl;
        }
        //2.绝对值，取反
        char[] chars = (xl + "").toCharArray();
        char[] y = new char[chars.length];
        int j = 0;
        for (int i = (chars.length - 1); i >= 0; i--) {
            y[j] = chars[i];
            j++;
        }
        returnStr += String.valueOf(y);
        returnNum = Long.parseLong(returnStr);
        //3.是否出边界
        if (returnNum >= Integer.MAX_VALUE || returnNum <= Integer.MIN_VALUE) {
            return 0;
        }
        return (int) returnNum;
    }

    //快速的方法
    public static int reverse1(int x) {
        int res = 0;
        int last = 0;
        while (x != 0) {
            //每次取末尾数字
            int tmp = x % 10;
            last = res;
            res = res * 10 + tmp;
            //判断整数溢出
            if (last != res / 10) {
                return 0;
            }
            x /= 10;
        }
        return res;
    }

}
