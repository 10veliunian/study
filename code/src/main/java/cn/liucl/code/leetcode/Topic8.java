package cn.liucl.code.leetcode;

import java.math.BigDecimal;

public class Topic8 {

    public static void main(String[] args) {
        System.out.println(myAtoi("-2000000000000000000000000000000"));
        System.out.println(myAtoi1("-2000000000000000000000000000000"));
    }

    /**
     * 字符串转换整数 (atoi)
     * 请你来实现一个 myAtoi(string s) 函数，使其能将字符串转换成一个 32 位有符号整数（类似 C/C++ 中的 atoi 函数）。
     * <p>
     * 函数 myAtoi(string s) 的算法如下：
     * 读入字符串并丢弃无用的前导空格
     * 检查下一个字符（假设还未到字符末尾）为正还是负号，读取该字符（如果有）。 确定最终结果是负数还是正数。 如果两者都不存在，则假定结果为正。
     * 读入下一个字符，直到到达下一个非数字字符或到达输入的结尾。字符串的其余部分将被忽略。
     * 将前面步骤读入的这些数字转换为整数（即，"123" -> 123， "0032" -> 32）。如果没有读入数字，则整数为 0 。必要时更改符号（从步骤 2 开始）。
     * 如果整数数超过 32 位有符号整数范围 [−231,  231 − 1] ，需要截断这个整数，使其保持在这个范围内。具体来说，小于 −231 的整数应该被固定为 −231 ，大于 231 − 1 的整数应该被固定为 231 − 1 。
     * 返回整数作为最终结果。
     */

    public static int myAtoi(String s) {
        //去掉空格
        char[] chars = s.trim().toCharArray();
        StringBuilder s1 = new StringBuilder();
        int re = 0;boolean is0 = false;
        for (int i = 0; i < chars.length; i++) {
            if (i == 0 && (chars[0] == '+' || chars[0] == '-')) {
                s1.append(chars[i]);
            }else if (chars[i] >= 48 && chars[i] < 58) {
                if (!is0 && chars[i]!=48){
                    is0 = true;
                }
                if (is0){
                    s1.append(chars[i]);
                }
            } else if (s1.length() == 0) {
                return re;
            } else {
                break;
            }
        }
        if (s1.length() == 0) {
            return re;
        } else if (s1.length() == 1 && (chars[0] == '+' || chars[0] == '-')) {
            return re;
        }
        BigDecimal l =new BigDecimal(s1.toString());
        //-1表示小于,0是等于,1是大于.
        if (l.compareTo(new BigDecimal(Integer.MAX_VALUE)) > 0) {
            re = Integer.MAX_VALUE;
        } else if (l.compareTo(new BigDecimal(Integer.MIN_VALUE))<0) {
            re = Integer.MIN_VALUE;
        } else {
            re = l.intValue();
        }
        return re;
    }
    //别人的写法
    public static int myAtoi1(String s) {
        int index = 0;
        int res = 0;
        char[] chars = s.toCharArray();
        if (s.length() == 0) {
            return 0;
        }
        //去除空格
        while (index < chars.length && chars[index] == ' ') {
            index ++;
        }
        if (index == s.length()) {
            return 0;
        }
        //加减号（1为正，-1为负）
        int sign = 1;
        if (chars[index] == '+') {
            index ++;
        } else if (chars[index] == '-') {
            sign = -sign;
            index ++;
        }
        //剩余转化为数字
        while (index < chars.length && chars[index] >= '0' && chars[index] <= '9') {
            int last = res;
            //一位一位往上加
            res = res * 10 + chars[index] - '0';
            index ++;
            //当前整数大于int的取值范围，则取[−2^31,  2^31 − 1]
            if (last != res / 10) {
                return sign > 0 ? 2147483647: -2147483648;
            }
        }
        return sign * res;
    }
}
