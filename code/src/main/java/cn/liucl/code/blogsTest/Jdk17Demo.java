package cn.liucl.code.blogsTest;

/**
 * @description: JDK 8 -> 17 的升级
 * @author: liuchengliang01
 * @time: 2023/2/2 18:29
 */
public class Jdk17Demo {

    public static void main(String[] args) {

        System.out.println("计算：" + calc(1, 2, "-"));

        System.out.println("json字符串：" + jsonString());
        try {
            System.out.println("空指针异常:" + getStr());
        } catch (Exception e) {
            System.out.println("空指针异常:" + e.getMessage());
        }

        User recordUser = new User("古时的风筝", 18);
        System.out.println(recordUser.name());
        System.out.println(recordUser.distance());
    }

    //从 jDK 14开始，对switch 表达式进行了增强
    public static int calc(int a, int b, String operation) {
        var result = switch (operation) {
            case "+" -> a + b;
            case "-" -> a - b;
            case "*" -> a * b;
            case "/" -> {
                System.out.println(a + "," + b);
                yield a / b;
            }
            default -> a;
        };
        return result;
    }

    //从 JDK 13开始，支持三引号字符串了，所以再有上面的 JSON 字符串的时候，就可以直接这样声明了。
    public static String jsonString() {
        return """
                {
                  "name": "古时的风筝",
                  "age": 18
                }
                """;
    }

    //空指针异常    出现异常的具体方法和原因都一目了然
    public static String getStr() {
        String s = null;
        String s1 = s.toLowerCase();
        return s1;
    }

    //从Java 16开始，我们将多一个关键词record 它也可以用来定义类。record关键词的引入，主要是为了提供一种更为简洁、紧凑的final类的定义方式。
    public record User(String name, Integer age) {
        int distance(){
            return name.length() - age;
        }
    }
}
