package cn.liucl.code.blogsTest.entry;

import lombok.Data;

/**
 * @description:
 * @author: liuchengliang01
 * @time: 2023/3/2 17:37
 */
@Data
public class Address {

    private String province;
    private String city;
    private String area;

}
