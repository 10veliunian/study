package cn.liucl.code.blogsTest.entry;

import lombok.Data;

@Data
public class User {

    private String name;
    private int age;
    private String phone;
    private Address address;

}
