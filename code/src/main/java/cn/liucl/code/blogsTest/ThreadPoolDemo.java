package cn.liucl.code.blogsTest;

import org.springframework.scheduling.concurrent.CustomizableThreadFactory;

import java.util.concurrent.*;

/**
 * @description: 线程池的demo
 * 日常开发中，为了更好管理线程资源，减少创建线程和销毁线程的资源损耗，我们会使用线程池来执行一些异步任务。但是线程池使用不当，就可能会引发生产事故。今天田螺哥跟大家聊聊线程池的10个坑。大家看完肯定会有帮助的~
 * 1.线程池默认使用无界队列，任务过多导致OOM
 * 2.线程创建过多，导致OOM
 * 3.共享线程池，次要逻辑拖垮主要逻辑
 * 4.线程池拒绝策略的坑
 * 5.Spring内部线程池的坑
 * 6.使用线程池时，没有自定义命名
 * 7.线程池参数设置不合理
 * 8.线程池异常处理的坑
 * 9.使用完线程池忘记关闭
 * 10.ThreadLocal与线程池搭配，线程复用，导致信息错乱。
 * @author: liuchengliang01
 * @time: 2023/2/7 15:38
 */
public class ThreadPoolDemo {

//        public static void main(String[] args) {
//            ExecutorService executor = Executors.newFixedThreadPool(10);
//            for (int i = 0; i < Integer.MAX_VALUE; i++) {
//                executor.execute(() -> {
//                    try {
//                        Thread.sleep(10000);
//                    } catch (InterruptedException e) {
//                        //do nothing
//                    }
//                });
//            }
//        }

    public static void main(String[] args) throws Exception {
        ThreadPoolExecutor executorOne = new ThreadPoolExecutor(5, 5, 1,
                TimeUnit.MINUTES, new ArrayBlockingQueue<>(20), new CustomizableThreadFactory("Tianluo-Thread-pool"));
        for (int i = 0; i < 5; i++) {
            executorOne.submit(() -> {
                System.out.println("current thread name" + Thread.currentThread().getName());
                try {
                    Object object = null;
                    System.out.print("result## " + object.toString());
                } catch (Exception e) {
                    System.out.println("异常了" + e);
                }
            });
        }
    }

}
